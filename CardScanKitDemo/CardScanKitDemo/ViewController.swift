import UIKit
import CardScanKit

class ViewController: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()

        let scanView = CardScanView(frame: view.bounds)
        scanView.translatesAutoresizingMaskIntoConstraints = false
        scanView.onCardDetected = { card in
            print("\(Date()) \(card)")
        }

        view.addSubview(scanView)
        NSLayoutConstraint.activate([
            scanView.leftAnchor.constraint(equalTo: view.leftAnchor),
            scanView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            scanView.rightAnchor.constraint(equalTo: view.rightAnchor),
            scanView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor)
        ])

    }
}
