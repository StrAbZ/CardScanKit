import Foundation

extension String {
    var isOnlyNumber: Bool {
        let regexp = #"^[0-9]*$"#
        return self.range(of: regexp, options: .regularExpression) != nil
    }

    var isPotentialExpiryDate: Bool {
        let regexp = #"^[0-9]{2}/[0-9]{2}$"#
        return self.range(of: regexp, options: .regularExpression) != nil
    }
}
