import AVFoundation

class CardScanKitVideo: NSObject {
    private static let videoQueueName = "cardscankit.queue.video"

    private let captureSession: AVCaptureSession
    private let device: AVCaptureDevice?
    private var videoInput: AVCaptureDeviceInput?
    private var videoOutput: AVCaptureVideoDataOutput?
    private let scanner: CardScanKitScanner

    let previewLayer: AVCaptureVideoPreviewLayer
    var processedImage: ((CardScanKitImage) -> Void)?

    override init() {
        self.captureSession = AVCaptureSession()
        self.device = AVCaptureDevice.default(for: .video)
        self.previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        self.scanner = CardScanKitScanner()
    }

    func starSession() {
        addOutput()
        DispatchQueue.global(qos: .background).async {
            self.captureSession.startRunning()
        }
    }

    func stopSession() {
        captureSession.stopRunning()
        removeOutput()
    }

    private func addOutput() {
        guard let device = device else { return }
        videoInput = try? AVCaptureDeviceInput(device: device)

        guard let videoInput = videoInput else { return }
        captureSession.addInput(videoInput)

        videoOutput = AVCaptureVideoDataOutput()
        let videoSettings = [kCVPixelBufferPixelFormatTypeKey as NSString: NSNumber(value: kCVPixelFormatType_32BGRA)] as [String: Any]
        videoOutput?.videoSettings = videoSettings
        videoOutput?.setSampleBufferDelegate(self, queue: DispatchQueue(label: CardScanKitVideo.videoQueueName))
        guard let videoOutput = videoOutput else { return }
        captureSession.addOutput(videoOutput)

        guard let connection = videoOutput.connection(with: .video), connection.isVideoOrientationSupported else {
            return
        }

        connection.videoOrientation = .portrait
    }

    private func removeOutput() {
        guard let videoOutput = videoOutput else { return }
        captureSession.removeOutput(videoOutput)
        videoOutput.setSampleBufferDelegate(nil, queue: nil)
        guard let videoInput = videoInput else { return }
        captureSession.removeInput(videoInput)
    }
}

extension CardScanKitVideo: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }

        let image = CardScanKitImage(withImageBuffer: imageBuffer, scanner: scanner)
        image.extract()

        processedImage?(image)
    }
}
