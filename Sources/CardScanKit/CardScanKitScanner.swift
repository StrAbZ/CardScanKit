import Foundation
import Vision

class CardScanKitScanner {
    var cardInfo: Card?
    var done: Bool = false

    private func checkExpiry(from string: String) -> [String]? {
        guard string.isPotentialExpiryDate, string.count == 5 else { return nil }
        let expiryComponents = string.components(separatedBy: "/")
        guard expiryComponents.count == 2 else { return nil }
        return expiryComponents
    }

    private func checkNumber(from string: String) -> String? {
        guard string.isOnlyNumber, string.count >= 13, string.count <= 16 else { return nil }
        return string
    }

    func extract(from textObservations: [VNRecognizedTextObservation]) {
        if done { return }

        var cardNumber: String = ""
        var cardExpirationMonth: String = ""
        var cardExpirationYear: String = ""

        let values = textObservations.flatMap { $0.topCandidates(10) }

        for value in values {
            let cleaned = value.string.trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: " ", with: "")
            if cleaned.isOnlyNumber, cleaned.count >= 13, cleaned.count <= 16 {
                cardNumber = cleaned
            }

            if let potentialCardNumber = checkNumber(from: cleaned) {
                cardNumber = potentialCardNumber
            }

            if let potentialExpiry = checkExpiry(from: cleaned) {
                cardExpirationMonth = potentialExpiry[0]
                cardExpirationYear = potentialExpiry[1]
            }
        }

        if !cardNumber.isEmpty && !cardExpirationMonth.isEmpty && !cardExpirationYear.isEmpty {
            cardInfo = Card(number: cardNumber, expiryMonth: cardExpirationMonth, expiryYear: cardExpirationYear)
            if cardInfo?.scheme != .unknown {
                #if !DEBUG
                done = true
                #endif
            }
        }
    }
}

