import Foundation

public struct Card {
    public let number: String
    public let expiryMonth: String
    public let expiryYear: String
    public let scheme: Scheme

    init(number: String, expiryMonth: String, expiryYear: String) {
        self.number = number
        self.expiryMonth = expiryMonth
        self.expiryYear = expiryYear

        self.scheme = Scheme(number: number)
    }
}

extension Card: CustomStringConvertible {
    public var description: String {
        var description: String = "\(type(of: self)){ "
        let selfMirror = Mirror(reflecting: self)
        for child in selfMirror.children {
            if let propertyName = child.label {
                description += "\(propertyName): \(child.value), "
            }
        }
        description = String(description.dropLast(2))
        description += " }"
        return description
    }
}
