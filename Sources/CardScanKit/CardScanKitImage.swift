import CoreImage
import Vision

class CardScanKitImage {
    private let imageBuffer: CVImageBuffer
    let scanner: CardScanKitScanner
    var cardInfo: Card?

    private lazy var request: VNRecognizeTextRequest = {
        let request = VNRecognizeTextRequest()
        request.recognitionLevel = .accurate
        request.usesLanguageCorrection = false
        return request
    }()

    init(withImageBuffer: CVImageBuffer, scanner: CardScanKitScanner) {
        self.imageBuffer = withImageBuffer
        self.scanner = scanner
    }

    func extract() {
        let ciImage = CIImage(cvImageBuffer: imageBuffer)
        let imageRequestHandler = VNImageRequestHandler(ciImage: ciImage, options: [:])
        try? imageRequestHandler.perform([request])

        guard let results = request.results, !results.isEmpty else {
            return
        }

        scanner.extract(from: results)
        if scanner.done {
            cardInfo = scanner.cardInfo
        }
    }
}
