import Foundation

public enum Scheme: CaseIterable, Equatable {
    case visa
    case mastercard
    case unknown
    
    init(number: String) {
        for scheme in Scheme.allCases {
            if let regexp = scheme.regexp, number.range(of: regexp, options: .regularExpression) != nil {
                self = scheme
                return
            }
        }
        
        self = .unknown
    }
    
    var regexp: String? {
        switch self {
        case .visa:
            return #"^4"#
        case .mastercard:
            return #"^(2[3-7]|22[2-9]|5[1-5])"#
        case .unknown:
            return nil
        }
    }
}

