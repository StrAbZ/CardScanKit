import UIKit
import AVFoundation
import Vision

public class CardScanView: UIView {
    private lazy var videoLayer = CardScanKitVideo()

//    private lazy var cardScanKit = CardScanKit()
//
//    private let captureSession = AVCaptureSession()
//    private let device = AVCaptureDevice.default(for: .video)
//    private let videoOutput = AVCaptureVideoDataOutput()

//    private var detectionLayer: CALayer? = nil

    public var onCardDetected: ((Card) -> Void)?

//    public var detectionLayerColor: UIColor = .black
//    public var detectionLayerBorderWidth: CGFloat = 2.0

//    private lazy var previewView: PreviewView = {
//        return PreviewView(frame: frame)
//    }()

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }

//    func setupLayers() {
//        detectionLayer = CALayer()
//        detectionLayer?.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
//        layer.addSublayer(detectionLayer!)
//    }

//    private func setupCallbacks() {
//        cardScanKit.detectionBoxesUpdated = { [weak self] boxes in
//            guard let self = self else { return }
//            DispatchQueue.main.async {
//                if boxes.isEmpty {
//                    self.detectionLayer?.sublayers?.removeAll()
//                } else {
//                    for box in boxes {
//                        box.borderColor = self.detectionLayerColor.cgColor
//                        box.borderWidth = self.detectionLayerBorderWidth
//                        self.detectionLayer?.addSublayer(box)
//                    }
//                }
//            }
//        }
//        cardScanKit.cardDetected = { [weak self] card in
//            self?.onCardDetected?(card)
//        }
//    }

    private func setupCallbacks() {
        videoLayer.processedImage = { [weak self] image in
            if image.scanner.done {
                guard let card = image.scanner.cardInfo else { return }
                self?.stop()
                self?.onCardDetected?(card)
            }
        }
    }

    private func start() {
        videoLayer.starSession()
//        DispatchQueue.global(qos: .background).async {
//            self.captureSession.startRunning()
//        }
    }

    private func stop() {
//        captureSession.stopRunning()
        videoLayer.stopSession()
    }

    private func setup() {
        videoLayer.previewLayer.frame = bounds
        layer.addSublayer(videoLayer.previewLayer)
//        setupDevice()
//        setupSession()
//        setupVideoOutput()
//        setupLayers()
        setupCallbacks()
        start()

//        let widht = UIScreen.main.bounds.width - (UIScreen.main.bounds.width * 0.2)
//        let height = widht - (widht * 0.45)
//        let viewX = (UIScreen.main.bounds.width / 2) - (widht / 2)
//        let viewY = (UIScreen.main.bounds.height / 2) - (height / 2) - 100
//        let viewGuide = PartialTransparentView(rectsArray: [CGRect(x: viewX, y: viewY, width: widht, height: height)])
//
//        addSubview(viewGuide)
//        viewGuide.translatesAutoresizingMaskIntoConstraints = false
//        viewGuide.leftAnchor.constraint(equalTo: leftAnchor, constant: 0).isActive = true
//        viewGuide.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 0).isActive = true
//        viewGuide.topAnchor.constraint(equalTo: topAnchor, constant: 0).isActive = true
//        viewGuide.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0).isActive = true
//        bringSubviewToFront(viewGuide)
    }
}

//class PartialTransparentView: UIView {
//    var rectsArray: [CGRect]?
//
//    convenience init(rectsArray: [CGRect]) {
//        self.init()
//
//        self.rectsArray = rectsArray
//
//        backgroundColor = UIColor.black.withAlphaComponent(0.6)
//        isOpaque = false
//    }
//
//    override func draw(_ rect: CGRect) {
//        backgroundColor?.setFill()
//        UIRectFill(rect)
//
//        guard let rectsArray = rectsArray else {
//            return
//        }
//
//        for holeRect in rectsArray {
//            let path = UIBezierPath(roundedRect: holeRect, cornerRadius: 10)
//
//            let holeRectIntersection = rect.intersection(holeRect)
//
//            UIRectFill(holeRectIntersection)
//
//            UIColor.clear.setFill()
//            UIGraphicsGetCurrentContext()?.setBlendMode(CGBlendMode.copy)
//            path.fill()
//        }
//    }
//}
