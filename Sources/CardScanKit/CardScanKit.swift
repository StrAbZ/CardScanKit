import Vision
import CoreImage
import QuartzCore
import UIKit

class CardScanKit {
    init() {}

    private var cardNumber: String?

    lazy var cardNumberLayer: CALayer = {
        let layer = CALayer()
        layer.borderWidth = 3.0
        layer.borderColor = CGColor.init(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        layer.cornerRadius = 4
        return layer
    }()


    private lazy var request: VNRecognizeTextRequest = {
        let request = VNRecognizeTextRequest()
        request.recognitionLevel = .accurate
        request.usesLanguageCorrection = false
        return request
    }()

    private var boxes: [CALayer] = []
    var detectionBoxesUpdated: (([CALayer]) -> Void)?
    var cardDetected: ((Card) -> Void)?

    private func rectFromBoundingBox(_ box: VNRectangleObservation, `in` rect: CGRect) -> CGRect {
        let objectBounds = VNImageRectForNormalizedRect(box.boundingBox, Int(rect.size.width), Int(rect.size.height))
        let transformedBounds = CGRect(x: objectBounds.minX, y: rect.size.height - objectBounds.maxY, width: objectBounds.maxX - objectBounds.minX, height: objectBounds.maxY - objectBounds.minY)
        return transformedBounds
    }

    private let sharedContext = CIContext(options: [.useSoftwareRenderer : false])

    private func resizedImage(_ image: CIImage, scale: CGFloat, aspectRatio: CGFloat) -> CIImage? {
        let filter = CIFilter(name: "CILanczosScaleTransform")
        filter?.setValue(image, forKey: kCIInputImageKey)
        filter?.setValue(scale, forKey: kCIInputScaleKey)
        filter?.setValue(aspectRatio, forKey: kCIInputAspectRatioKey)

        return filter?.outputImage
//        guard let outputCIImage = filter?.outputImage,
//            let outputCGImage = sharedContext.createCGImage(outputCIImage,
//                                                            from: outputCIImage.extent)
//        else {
//            return nil
//        }
//
//        return UIImage(cgImage: outputCGImage)
    }

    func extractData(from imageBuffer: CVImageBuffer, inRect rect: CGRect) {
        boxes.removeAll()

        let ciImage = CIImage(cvImageBuffer: imageBuffer)

        let targetSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        let scale = targetSize.width / ciImage.extent.width
//        let scale = targetSize.height / ciImage.extent.height
//        let aspectRatio = targetSize.width / (ciImage.extent.width * scale)
        let resizedImage = resizedImage(ciImage, scale: scale, aspectRatio: 1.0)

        let widht = UIScreen.main.bounds.width - (UIScreen.main.bounds.width * 0.2)
        let height = widht - (widht * 0.45)
        let viewX = (UIScreen.main.bounds.width / 2) - (widht / 2)
        let viewY = (UIScreen.main.bounds.height / 2) - (height / 2) - 100 + height

        let resizeFilter = CIFilter(name: "CILanczosScaleTransform")!

        // Desired output size
//        let targetSize = CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)

        // Compute scale and corrective aspect ratio
//        let scale = targetSize.height / ciImage.extent.height
//        let aspectRatio = targetSize.width / ciImage.extent.width

        // Apply resizing
        resizeFilter.setValue(ciImage, forKey: kCIInputImageKey)
        resizeFilter.setValue(scale, forKey: kCIInputScaleKey)
        resizeFilter.setValue(1.0, forKey: kCIInputAspectRatioKey)
        let outputImage = resizeFilter.outputImage

        let croppedImage = resizedImage!.cropped(to: CGRect(x: viewX, y: viewY, width: widht, height: height))

        let uiImage = UIImage(ciImage: ciImage)
        let croppedUIImage = UIImage(ciImage: croppedImage)

        let imageRequestHandler = VNImageRequestHandler(ciImage: croppedImage, options: [:])
        try? imageRequestHandler.perform([request])

        guard let results = request.results, results.count > 0 else {
            cardNumber = nil
            return
        }
        let values = results.flatMap { $0.topCandidates(10) }

        for value in values {
            let cleaned = value.string.trimmingCharacters(in: .whitespacesAndNewlines).replacingOccurrences(of: " ", with: "")

            if cleaned.isOnlyNumber, cleaned.count >= 13, cleaned.count <= 16 {
                cardNumber = cleaned
                if let box = try? value.boundingBox(for: value.string.startIndex..<value.string.endIndex) {
                    self.cardNumberLayer.frame = self.rectFromBoundingBox(box, in: rect)
                    boxes.append(self.cardNumberLayer)
                }
            }

            // TODO: Check for date
            // TODO: Check for Name
        }

        detectionBoxesUpdated?(boxes)

        if let cardNumber {
//            cardDetected?(Card(number: cardNumber, expiryDate: Date()))
        }
    }
}
