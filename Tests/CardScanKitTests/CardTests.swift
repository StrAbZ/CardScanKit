@testable import CardScanKit
import XCTest

final class CardTests: XCTestCase {
    func testVisa() {
        let card = Card(number: "4242424242424242", expiryMonth: "04", expiryYear: "24")
        XCTAssertEqual(card.scheme, .visa)
    }

    func testMastercard() {
        let card = Card(number: "5355000000000000", expiryMonth: "04", expiryYear: "24")
        XCTAssertEqual(card.scheme, .mastercard)
    }

    func testUnknown() {
        let card = Card(number: "9876000000000000", expiryMonth: "04", expiryYear: "24")
        XCTAssertEqual(card.scheme, .unknown)
    }
}
