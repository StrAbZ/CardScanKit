@testable import CardScanKit
import XCTest

final class SchemeTests: XCTestCase {
    func testVisaScheme() {
        let number = "4811909089897878"
        XCTAssertEqual(Scheme(number: number), .visa)
    }

    func testMastercardScheme() {
        var number = "2300000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "2400000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "2500000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "2600000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "2700000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "2220000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "2230000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "2240000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "2250000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "2260000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "2270000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "2280000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "2290000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "5100000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "5200000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "5300000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "5400000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)

        number = "5500000000000000"
        XCTAssertEqual(Scheme(number: number), .mastercard)
    }

    func testUnknownScheme() {
        var number = "2100000000000000"
        XCTAssertEqual(Scheme(number: number), .unknown)

        number = "2210000000000000"
        XCTAssertEqual(Scheme(number: number), .unknown)

        number = "5600000000000000"
        XCTAssertEqual(Scheme(number: number), .unknown)
    }
}
