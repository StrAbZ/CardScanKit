@testable import CardScanKit
import XCTest

final class StringExtentionsTests: XCTestCase {
    func testIsOnlyNumber() {
        var str = "BONJOUR MONDE"
        XCTAssertFalse(str.isOnlyNumber)

        str = "HELLO 33"
        XCTAssertFalse(str.isOnlyNumber)

        str = "44 33"
        XCTAssertFalse(str.isOnlyNumber)

        str = "4433"
        XCTAssertTrue(str.isOnlyNumber)
    }

    func testIsPotentialExpiryDate() {
        var str = "BONJOUR MONDE"
        XCTAssertFalse(str.isPotentialExpiryDate)

        str = "HELLO33"
        XCTAssertFalse(str.isPotentialExpiryDate)

        str = "BONJOUR/MONDE"
        XCTAssertFalse(str.isPotentialExpiryDate)

        str = "0924"
        XCTAssertFalse(str.isPotentialExpiryDate)

        str = "09/24"
        XCTAssertTrue(str.isPotentialExpiryDate)
    }
}
